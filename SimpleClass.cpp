// DateClass.cc
// Program to demonstrate the definition of a simple class
// and member functions

#include <iostream>
using namespace std;


// Declaration of Date class
class Date {

    public:  
        Date(int, int, int, bool);
        void set(int, int, int);
        void set(int[]);
        void print();
    
    public:
        bool IsPM;

    private:
        int year;
        int month;
        int day;
        
};


int main()
{
   // Declare today to be object of class Date
   // Values are automatically intialised by calling constructor function
   Date today(1, 9, 1999, false);

   cout << "This program was written on ";
   today.print(); 

   cout << "This program was modified on ";
   today.set(5,10,1999);
   today.print();
   
   cout << "Other modification on ";
   int fecha[3] = {6, 11, 2000};
   today.set(fecha);
   today.print();
   
   
   if( today.IsPM ) today.print();

   return 0;
}

// Date constructor function definition
Date::Date(int d, int m, int y, bool p)
{
  if(d>0 && d<31) day = d;
  if(m>0 && m<13) month = m;
  if(y>0) year =y;
  IsPM = p;
  
}

// Date member function definitions
void Date::set(int d, int m, int y)
{
  if(d>0 && d<31) day = d;
  if(m>0 && m<13) month = m;
  if(y>0) year =y;
}

// Date member function definitions
void Date::set(int datos[])
{
  if(datos[0]>0 && datos[0]<31) day = datos[0];
  if(datos[1]>0 && datos[1]<13) month = datos[1];
  if(datos[2]>0) year = datos[2];
}


void Date::print()
{
  cout << day << "-" << month << "-" << year << endl;
}


