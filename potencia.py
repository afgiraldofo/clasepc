def potencia(b,n):
    
    # Caso base
    print "b = " + str(b) + " , n = " + str(n)
    
    if n <= 0:
        return 1
 
    # n par
    if n % 2 == 0:
        pot = potencia(b, n/2)
        return pot * pot
    # n impar
    else:
        pot = potencia(b, (n-1)/2)
        return pot * pot * b

